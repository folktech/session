from django.contrib import admin
from .forms import TrackForm
from .models import Event, Session, Track, Tune


class TrackInline(admin.TabularInline):
    model = Track
    form = TrackForm
    extra = 0
    fields = ('tune',)
    verbose_name_plural = "Tunes"


class EventInline(admin.TabularInline):
    model = Event
    extra = 0
    fields = ('date',)
    verbose_name_plural = "Dates"


class SessionAdmin(admin.ModelAdmin):
    list_display = ('name', 'location',)
    list_filter = ('name',)
    readonly_fields = ('created',)
    ordering = ['-created']
    search_fields = ('name',)
    inlines = [EventInline]


class EventAdmin(admin.ModelAdmin):
    list_display = ('session', 'date',)
    list_filter = ('session', 'date',)
    readonly_fields = ('created',)
    ordering = ['-created']
    search_fields = ('session', 'date',)
    inlines = [TrackInline]

    def save_formset(self, request, form, formset, change):
        if change:
            for f in formset.forms:
                obj = f.instance 
                obj.author = request.user
                obj.save()
            formset.save()


class TrackAdmin(admin.ModelAdmin):
    list_display = ('tune', 'event',)
    list_filter = ('event', 'author',)
    readonly_fields = ('created', 'author',)
    ordering = ['-created']
    search_fields = ('tune', 'event',)
    form = TrackForm

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.save()


class TuneAdmin(admin.ModelAdmin):
    list_display = ('name', 'tune_type', 'meter', 'mode',)
    list_filter = ('tune_type', 'meter', 'mode',)
    readonly_fields = ('sid', 'setting', 'created', 'abc_full',)
    ordering = ['-created']
    search_fields = ('name', 'tune_type',)


admin.site.register(Session, SessionAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Track, TrackAdmin)
admin.site.register(Tune, TuneAdmin)
