from django.urls import path
from . import views

urlpatterns = [
    path('sessions', views.session_list, name='session_list'),
    path('sessions/<int:pk>/', views.session_detail, name='session_detail'),
    path('', views.track_list, name='track_list'),
    path('track/<int:pk>/', views.track_detail, name='track_detail'),
    path('track/<int:pk>/edit/', views.track_edit, name='track_edit'),
    path('track/new/', views.track_new, name='track_new'),
    path('tunes/<int:pk>/', views.tune_detail, name='tune_detail'),
    path('tune-autocomplete/', views.TuneAutocomplete.as_view(), name='tune-autocomplete'),
]
