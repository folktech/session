from dal import autocomplete
from django import forms

from .models import Track, Tune

class TrackForm(forms.ModelForm):
    class Meta:
        model = Track
        fields = ('tune', 'event',)
        widgets = {
            'tune': autocomplete.ModelSelect2(url='tune-autocomplete', attrs={'data-minimum-input-length': 2})
        }

# class TrackToEventForm(forms.ModelForm):
#     class Meta:
#         model = Track
#         fields = ('tune',)
