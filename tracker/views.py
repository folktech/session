from dal import autocomplete
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone

from .forms import TrackForm
from .models import Session, Event, Track, Tune


class TuneAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Tune.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


def session_list(request):
    sessions = Session.objects.order_by('created')
    return render(request, 'tracker/session_list.html', {'sessions': sessions})


def session_detail(request, pk):
    if request.method == "POST":
        form = TrackForm(request.POST)
        if form.is_valid():
            track = form.save(commit=False)
            track.author = request.user
            track.save()
            return redirect('session_detail', pk=pk)
    else:
        session = get_object_or_404(Session, pk=pk)
        events = Event.objects.filter(session=session).order_by('created')
        tracks = Track.objects.filter(event__in=events).order_by('created')
        # This can be done with Django ORM .distinct() if not using sqlite backend.
        tunes = list(dict.fromkeys([track.tune for track in tracks]))
        form = TrackForm()
        return render(
            request,
            'tracker/session_detail.html',
            {'session': session, 'events': events, 'tunes': tunes, 'form': form}
        )
    

def track_list(request):
    tracks = Track.objects.filter(created__lte=timezone.now()).order_by('created')
    return render(request, 'tracker/track_list.html', {'tracks': tracks})


def track_detail(request, pk):
    track = get_object_or_404(Track, pk=pk)
    return render(request, 'tracker/track_detail.html', {'track': track})


@login_required
def track_new(request):
    if request.method == "POST":
        form = TrackForm(request.POST)
        if form.is_valid():
            track = form.save(commit=False)
            track.author = request.user
            track.save()
            return redirect('track_detail', pk=track.pk)
    else:
        form = TrackForm()
    return render(request, 'tracker/track_edit.html', {'form': form})


@login_required
def track_edit(request, pk):
    track = get_object_or_404(Track, pk=pk)
    if request.method == "POST":
        form = TrackForm(request.POST, instance=track)
        if form.is_valid():
            track = form.save(commit=False)
            track.author = request.user
            track.save()
            return redirect('track_detail', pk=track.pk)
    else:
        form = TrackForm(instance=track)
    return render(request, 'tracker/track_edit.html', {'form': form})


def tune_detail(request, pk):
    tune = get_object_or_404(Tune, pk=pk)
    return render(request, 'tracker/tune_detail.html', {'tune': tune})
