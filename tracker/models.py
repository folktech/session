from django.conf import settings
from django.db import models
from django.utils import timezone


class Session(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Event(models.Model):
    session = models.ForeignKey('tracker.Session', on_delete=models.CASCADE, related_name='events')
    created = models.DateTimeField(default=timezone.now)
    date = models.DateField()

    def __str__(self):
        return f"{self.session} on {self.date}"


class Track(models.Model):
    tune = models.ForeignKey('tracker.Tune', on_delete=models.CASCADE, related_name='tracks')
    event = models.ForeignKey('tracker.Event', on_delete=models.CASCADE, related_name='tracks')
    created = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.tune} tracked at {self.event}"


tune_types = [
    ('polka',) * 2,
    ('slip jig',) * 2,
    ('strathspey',) * 2,
    ('reel',) * 2,
    ('jig',) * 2,
    ('waltz',) * 2,
    ('march',) * 2,
    ('barndance',) * 2,
    ('mazurka',) * 2,
    ('hornpipe',) * 2,
    ('three-two',) * 2,
    ('slide',) * 2,
]

modes = []
for letter in 'ABCDEFG':
    modes += [
        (letter,) * 2,
        (f'{letter}dorian',) * 2,
        (f'{letter}major',) * 2,
        (f'{letter}minor',) * 2,
        (f'{letter}mixolydian',) * 2,
    ]


def create_abc_full(name, tune_type, meter, mode, abc):
    return "\n".join([
        "X: 1",
        f"T: {name}",
        f"R: {tune_type}",
        f"M: {meter}",
        "L: 1/8",
        f"K: {mode}",
        f"{abc}"
    ])


class Tune(models.Model):
    name = models.CharField(max_length=200)
    tune_type = models.CharField(max_length=250, blank=False, choices=tune_types)
    meter = models.CharField(max_length=20, blank=True)
    mode = models.CharField(max_length=40, blank=False, choices=modes)
    abc = models.TextField(blank=True)
    abc_full = models.TextField(blank=True)
    sid = models.IntegerField(unique=False, blank=False, null=False)
    setting = models.IntegerField(unique=False, blank=False, null=False)
    created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        self.abc_full = create_abc_full(self.name, self.tune_type, self.meter, self.mode, self.abc)
        super(Tune, self).save(*args, **kwargs)
