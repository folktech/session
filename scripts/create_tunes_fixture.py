"""
Load all tunes from The Session into the tunes db table as a fixture. Run as follows:
    $ python manage.py runscript create_tunes_fixture
    $ python manage.py loaddata tunes.json
"""
import json
from pathlib import Path

import pandas as pd
import requests

from tracker.models import Tune, create_abc_full


def run():
    print(f"Running create_tunes_fixture.run() in directory {Path('.').resolve()}")
    # Download & load data from github.com/adactio/TheSession-data/
    print('Downloading tunes.json')
    tunes_url = "https://raw.githubusercontent.com/adactio/TheSession-data/master/json/tunes.json"
    response = requests.get(tunes_url)
    if response.status_code != 200:
        raise RuntimeError(f'Received response code {response.status_code} from raw.githubusercontent.com ({response.text})')

    # Load JSON -> DataFrame -> dict.
    print('Converting to fixture')
    tunes = (
            pd.DataFrame(response.json())
            .rename(columns={
                'type': 'tune_type',
                'tune': 'sid',
            })
            .drop(columns=['date', 'username'])
            # Keep first setting only
            .sort_values(['sid', 'setting'], ascending=True)
            .drop_duplicates(subset='sid', keep='first')
        )
    tunes['name'] = tunes.name.apply(lambda x: f'The {x[:-5]}' if x[-5:] == ', The' else x)
    tunes['abc_full'] = tunes.apply(lambda row: create_abc_full(row['name'], row['tune_type'], row['meter'], row['mode'], row['abc']), axis=1)
    tunes = tunes.to_dict(orient='records')

    # Export as Django fixture
    fixture = [{
        'model': 'tracker.tune',
        'pk': i,
        'fields': entry
        } for i, entry in enumerate(tunes, start=1)
    ]
    filepath = Path('./tracker') / 'fixtures' / 'tunes.json'
    print(f'Writing to {filepath.resolve()}')
    with open(filepath, 'w') as file:
        json.dump(fixture, file)

    # Delete all data in the tune table
    print('Deleting all rows in table: tunes')
    Tune.objects.all().delete()
