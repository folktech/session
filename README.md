# Session Tracker

### Deploy to Heroku
```
git push heroku master
heroku run python manage.py migrate
heroku ps:scale web=1
heroku open
heroku logs

# Wipe the database (helpful for tricky migrations during development when data isn't real)
heroku run python manage.py flush
heroku run python manage.py createsuperuser
```


### Load fixtures
**Beware:** `create_tunes_fixture.py` currently deletes all Tunes and related Tracks!
Do this on production from `heroku run bash`.
```
python manage.py runscript create_tunes_fixture
python manage.py loaddata tunes.json
```
